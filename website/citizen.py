from flask import Blueprint,redirect,request,url_for,render_template,flash
from werkzeug.security import generate_password_hash,check_password_hash
from flask_login import login_required,login_user,logout_user,UserMixin,current_user
from .mongo import *
from .methods import *



citizen = Blueprint("citizen",__name__)

class User(UserMixin):
    def __init__(self, data):
        self.data = data
    def get_id(self):
        return str(self.data['username'])


@citizen.route('/')
def home():
    if current_user.is_authenticated:
        if current_user.data['role'] == 'police':
            return redirect(url_for('police.home'))
        else:
            return render_template("/citizen/ctizenhome.html",user=current_user.data)
    else:
        return redirect(url_for('citizen.login'))

@citizen.route('/login',methods=["GET","POST"])
def login():
    if current_user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        username = request.form.get('username')
        password = request.form.get('pwd')
        user = citizens.find_one({'username':username})
        if user:
                if check_password_hash(user['password'],password):
                    loginuser = User(user)
                    login_user(loginuser, remember=request.form.get('remember'))
                    flash('Logged In Successfully!')
                    return redirect('/')
                else:
                    flash('Incorrect Password! Try Again')
        else:
            flash('Username does not exists.')
    return render_template("/citizen/login.html")

@citizen.route('/sign-up',methods = ["POST","GET"])
def sign_up():
    if current_user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        p2 = request.form.get('pwd2')
        data = {
            "username" : request.form.get('username'),
            "password" : request.form.get('pwd'),
            "name" : request.form.get('name'),
            "phone" : request.form.get('phone'),
            "role"  : "citizen",
            "points" : 0,
            "challan" : 0,
            "history" : [],
            "vehicle_no" : request.form.get('vehicle_no').upper(),
            "profile_slug" : None
        }
        profile = request.files['profile']
        if profile.filename == '':
            data['profile_slug'] = url_for('static',filename='/profileImgs/default.jpeg')
        elif allowed_file(profile.filename):
            filename = generate_filename(profile.filename,data['username'])
            profile.save('website/static/profileImgs/'+filename)
            data['profile_slug'] = url_for('static',filename='/profileImgs/'+filename)

        if data['password'] == p2:
            data['password'] = generate_password_hash(data['password'])
            if citizens.find_one({'username':data['username']}):
                flash('Username already exists.')
            else:
                citizens.insert_one(data)
                flash('Account Created Successfully!')
                return redirect(url_for('citizen.login'))
        else:
            flash('Password does not match.')
    return render_template("/citizen/login.html")
        

