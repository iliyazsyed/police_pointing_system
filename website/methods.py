from werkzeug.utils import secure_filename
def generate_filename(filename,username):
    filename = secure_filename(filename)
    ext = filename.split('.')[-1]
    return f"{username}.{ext}"

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.',1)[1].lower() in {'png','jpg','jpeg','gif'}

def calculate_pfor_user(data):
    points = 0
    for key in data:
        if data[key] == 'on':
            points+=1
    # print(points)
    if points < 5:
        return -(5-points)
    return points

def calculate_cfor_user(data):
    challan = 0
    for key in data:
        if data[key] != 'on':
            challan+=500
    return challan


def history_gen(data,points):
    checker = True
    for key in data:
        if data[key] != "on":
            checker =  False
    if checker:
        return ['All Docs ok',points]
    else:
        return ['Docs Missing',points]
    