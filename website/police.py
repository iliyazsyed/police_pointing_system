from bson import ObjectId
from flask import Blueprint,redirect,request,url_for,render_template,flash
from werkzeug.security import generate_password_hash
from .mongo import *
from flask_login import current_user
from .methods import *

police = Blueprint("police",__name__)

@police.route('/',methods=["GET","POST"])
def home():
    if current_user.is_authenticated:
        if current_user.data['role'] == 'citizen':
            return redirect(url_for('citizen.home'))
        else:
            if request.method == "POST":
                vno = request.form.get("vehicle_no")
                phone = request.form.get("phone")
                data = {
                    "insurance" : request.form.get("ins"),
                    "dl" : request.form.get("dl"),
                    "rc" : request.form.get("rc"),
                    "poll" : request.form.get("poll"),
                    "safety" : request.form.get("safe")
                }
                citizen_data = citizens.find_one({"phone":phone})
                police_data = citizens.find_one({"username":current_user.data['username']})
                points = calculate_pfor_user(data)
                challan = calculate_cfor_user(data)
                if citizen_data:
                    citizens.update_one({"phone":phone},{'$set':{'points':citizen_data['points']+points}})
                    citizens.update_one({"phone":phone},{'$set':{'challan':citizen_data['challan']+challan}})
                    citizens.update_one({"phone":phone},{'$push':{'history':history_gen(data,points)}})
                    citizens.update_one({"username":current_user.data['username']},{'$set':{"points":police_data['points']+1}})
                    citizens.update_one({"username":current_user.data['username']},{'$push':{'history':[vno,points]}})
                else:
                    flash("Citizen doesnot exist")
                return redirect(url_for('police.home'))

            return render_template("/police/home.html",user=current_user.data)
    else:
        return redirect(url_for('citizen.login'))
    
    
@police.route('/signup',methods = ["POST","GET"])
def sign_up():
    if current_user.is_authenticated:
        return redirect('/')
    if request.method == "POST":
        p2 = request.form.get('pwd2')
        data = {
            "username" : request.form.get('username'),
            "password" : request.form.get('pwd'),
            "name" : request.form.get('name'),
            "phone" : request.form.get('phone'),
            "role"  : "police",
            "points" : 0,
            "history" : [],
            "idNo" : request.form.get('idNo'),
            "idproof_slug" : None,
            "profile_slug" : None
        }
        profile = request.files['profile']
        if profile.filename == '':
            data['profile_slug'] = url_for('static',filename='/profileImgs/default.jpeg')
        elif allowed_file(profile.filename):
            filename = generate_filename(profile.filename,data['username'])
            profile.save('website/static/profileImgs/'+filename)
            data['profile_slug'] = url_for('static',filename='/profileImgs/'+filename)

        idproof = request.files['idproof']
        if idproof.filename == '':
            flash('ID Proof is required.')
            return render_template("/police/signup.html")
        elif allowed_file(idproof.filename):
            filename = generate_filename(idproof.filename,data['username'])
            profile.save('website/static/idproofs/'+filename)
            data['idproof_slug'] = url_for('static',filename='/idproofs/'+filename)

        if data['password'] == p2:
            data['password'] = generate_password_hash(data['password'])
            if citizens.find_one({'username':data['username']}):
                flash('Username already exists.')
            else:
                citizens.insert_one(data)
                flash('Account Created Successfully!')
                return redirect(url_for('citizen.login'))
        else:
            flash('Password does not match.')
    return render_template("/police/signup.html")

