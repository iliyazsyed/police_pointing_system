from flask import Flask,redirect,url_for,flash
from flask_login import LoginManager,current_user,logout_user,login_required
from .mongo import citizens
from .citizen import citizen,User
from .police import police
def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'channamereya'

    app.register_blueprint(citizen,url_prefix='/citizen')
    app.register_blueprint(police,url_prefix='/police')

    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = 'login'
    @login_manager.user_loader
    def load_user(username):
        data =  citizens.find_one({'username':username})
        return User(data)

    @app.route('/')
    def entry():
        if current_user.is_authenticated:
            if current_user.data['role'] == 'citizen':
                return redirect(url_for('citizen.home'))
            else:
                return redirect(url_for('police.home'))
        else:
            return redirect(url_for('citizen.login'))

    @app.route('/logout')
    @login_required
    def logout():
        logout_user()
        flash('Logged Out Successfully!')
        return redirect(url_for('citizen.login'))
    return app