function displayFlashAsToast() {
    var flashMessage = document.getElementById('flash-message').innerText;
    flashMessage = flashMessage.replace(/^\[|\]$/g, '').replace(/^'|'$/g, '');
    console.log(String(flashMessage))
    if (flashMessage !== "") {
        Toastify({
            text: flashMessage,
            duration: 3000, 
            gravity: "top", 
            position: "left",
        }).showToast();
    }
}

window.onload = displayFlashAsToast;