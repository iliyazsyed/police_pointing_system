from pymongo import MongoClient


def mongo_conn(uri):
    try:
        conn = MongoClient(uri)
        print("Connected to MongoDB")
        return conn
    except Exception as err:
        print(f"Error in your mongodb connection: {err}")



uri = "mongodb://localhost:27017"
client = mongo_conn(uri)
psp_sys = client["psp_sys"]
citizens = psp_sys["citizens"]
policeofc = psp_sys["police"]
